function filter(elements, callback) {
    if (elements == null || !Array.isArray(elements) || elements.length == 0) {
        return [];
    }
    const outPutArray = [];
    for (let index = 0; index < elements.length; index++) {
        if (callback(elements[index], index, elements) === true) {
            outPutArray.push(elements[index]);
        }
    }
    return outPutArray;
}


module.exports = filter;