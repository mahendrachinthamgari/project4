function map(elements, callback) {
    if (elements == null || !Array.isArray(elements) || elements.length == 0) {
        return [];
    }
    const mappedArray = [];
    for (let index = 0; index < elements.length; index++) {
        mappedArray.push(callback(elements[index], index, elements));
    }
    return mappedArray;
}


module.exports = map;