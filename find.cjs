function find(elements, callback) {
    if (elements == null || !Array.isArray(elements) || elements.length == 0) {
        return undefined;
    }
    for (let index = 0; index < elements.length; index++) {
        const boolean = callback(elements[index]);
        if (boolean) {
            return elements[index];
        }
    }
    return undefined;
}


module.exports = find;