function flatten(elements, depth = 1) {
    let flattenElements = [];
    function flat(element, innerDepth) {
        for (let index = 0; index < element.length; index++) {
            if (Array.isArray(element[index]) && innerDepth) {
                flat(element[index], innerDepth - 1);
            } else {
                if (element[index]) {
                    flattenElements.push(element[index]);
                }
            }
        }
    }
    flat(elements, depth);
    return flattenElements;
}


module.exports = flatten;