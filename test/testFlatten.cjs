const flatten = require("../flatten.cjs");


const items = [1, [2], [[3]], [[[4]]]];
const flattenItems = flatten(items, 2);
console.log(flattenItems);