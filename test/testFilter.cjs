const filter = require("../filter.cjs");
const items = require("./arrays.cjs");


function callback(element) {
    return element % 2;
}

const filtered = filter(items, callback);
console.log(filtered);