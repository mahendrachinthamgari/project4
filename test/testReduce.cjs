const reduce = require("../reduce.cjs");
const items = require("./arrays.cjs");


function callback(startingValue, element) {
    return startingValue + element;
}

const startingValue = 1;
const reduced = reduce(items, callback, startingValue);
console.log(reduced);