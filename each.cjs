function each(elements, callback) {
    if (elements == null || !Array.isArray(elements) || elements.length == 0) {
        return undefined;
    }
    for (let index = 0; index < elements.length; index++) {
        callback(elements[index], index);
    }
}


module.exports = each;